;; podwójne średniki do komentarzy, pojedyncze do wyłączania opcji
;;To run emacs without loading your personal init file, start emacs like this: emacs -q

;; Wczytujemy konfig klawiszem F9, nie trzeba wychodzić z Emacsa
(defun load-my-config ()
  (interactive)
    (load-file "~/.emacs"))
(global-set-key (kbd "<f9>") 'load-my-config)

;; http://stackoverflow.com/questions/586735/how-can-i-check-if-a-current-buffer-exists-in-emacs/2050989#2050988
;; http://stackoverflow.com/questions/2662655/automatically-closing-the-scratch-buffer/2662728#2662728
(if (get-buffer "*scratch*")  (kill-buffer "*scratch*"))

;; some kind of laziness in package.el is cousing the problem
;;http://stackoverflow.com/a/15497402/588759 http://stackoverflow.com/a/12055476/588759
(require 'package)

(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/"))
;; after adding new repo
;; M-x package-refresh-contents

(package-initialize)

(add-to-list 'load-path "~/elisp")

(setq inhibit-splash-screen t) ; nie pokazuje ekranu startowego
(setq make-backup-files nil) ; nie robi backupow
(menu-bar-mode -1) ; wylaczamy menubar na gorze
(transient-mark-mode t) ; widoczne co zaznaczylismy

;; Funkcja i bindowanie do zamkniecia okna i buforu jednoczesnie
;; thanks to piranha from #emacs at freenode
(defun kill-and-close-buffer () (interactive) (kill-buffer nil) (delete-window))
(global-set-key (kbd "C-x x") 'kill-and-close-buffer)

;; http://www.emacswiki.org/cgi-bin/wiki/InteractivelyDoThings
;; dopelnia wewnatrz nazw nie tylko poczatki, np przy otwieraniu plikow
;; #emacs@freenode 2008-03-06 
;; [11:26] <bob2> rofrol: I like it because it completes on strings
;; within the names, not just at the beginning
(require 'ido)
(ido-mode t)
;; otwieranie w textmate jest w fuzzy mode, co to jest?
;; http://groups.google.com/group/comp.emacs/msg/444300433515472b?dmode=source
;; opcja odpowiadająca to
; http://groups.google.com/group/comp.emacs/msg/444300433515472b?dmode=source
; (setq ido-enable-flex-matching t)

;; http://www.codecoffee.com/tipsforlinux/articles/033.html
;; Changes all yes/no questions to y/n type
(fset 'yes-or-no-p 'y-or-n-p)

;;Plynne przewijanie po jednej linii
(setq scroll-step 2)
;;alternatywnie mozna ustawic dwie zmienne
;;http://www.emacswiki.org/emacs-en/SmoothScrolling
;scroll-step 1
;scroll-conservatively 10000
;;jesli potrzeba ustawic margines dla przewijania
;;http://emacs.wordpress.com/2007/09/10/smooth-scrolling/

;;Utrzymywanie kursora w tej samej pozycji podczas przewijania stron
(setq scroll-preserve-screen-position t)

;; "http://snarfed.org/space/emacs page up page down"
;; page down przesuwa takze kursor, jesli dojedzie do konca strony
;; Page down/up move the point, not the screen.
;; In practice, this means that they can move the
;; point to the beginning or end of the buffer.
(global-set-key [next]
  (lambda () (interactive)
    (condition-case nil (scroll-up)
      (end-of-buffer (goto-char (point-max))))))

(global-set-key [prior]
  (lambda () (interactive)
    (condition-case nil (scroll-down)
      (beginning-of-buffer (goto-char (point-min))))))

;; pokaz nie widoczne znaki na końcu linii
;; http://trey-jackson.blogspot.com/2008/03/emacs-tip-12-show-trailing-whitespace.html
(setq-default show-trailing-whitespace t)

;; http://stackoverflow.com/questions/568885/show-tabs-with-a-different-character-emacs
;; http://www.emacswiki.org/emacs/WhiteSpace
(require 'whitespace)
(defun turn-on-whitespace () (whitespace-mode 1))
(add-hook 'find-file-hooks 'turn-on-whitespace)
;;(setq whitespace-style '(trailing tabs newline tab-mark newline-mark))
(setq whitespace-style '(tabs tab-mark))

;; włączanie outline-mode dla plików o nazwie TODO
;; * to poziom 1, ** to poziom 2, jeśli wykonane to dajemy spację przed gwiazdką
;; from Teach Yourself Emacs in 24 hours
;; http://edward.oconnor.cx/2006/03/poor-mans-todo-list
;; http://www.emacswiki.org/cgi-bin/wiki/OutlineMode
(setq auto-mode-alist (cons '("TODO" . outline-mode)auto-mode-alist))

;; utf-8
;; http://gentoo-wiki.com/HOWTO_Make_your_system_use_unicode/utf-8#Editors
(setq locale-coding-system 'utf-8)
(setq terminal-coding-system 'utf-8)
(setq keyboard-coding-system 'utf-8)
(setq selection-coding-system 'utf-8)
(setq prefer-coding-system 'utf-8)

;; http://www.xsteve.at/prg/emacs/.emacs.txt
;;http://www.bazon.net/mishoo/.emacs

;; dziala zwykle global
;(global-set-key "\M-g" 'newline-and-indent)

;; nie dziala zwykle local
;(local-set-key "\M-g" 'newline-and-indent)

;; dziala kbd
;(define-key global-map (kbd "M-g") 'newline-and-indent)

;; nie dziala kbd local
;(define-key local-map (kbd "M-g") 'newline-and-indent)

;; podobno enter jest zasloniety lokalna definicja, ale ponizsze nie dzial
;(local-set-key (kbd "C-m") 'newline-and-indent

;; za to to dziala
;(global-set-key "\C-m" 'newline-and-indent)

;; Przydatne do pisania programów
(global-set-key (kbd "C-m") 'newline-and-indent)


;; http://stackoverflow.com/questions/5710334/how-can-i-get-mouse-selection-to-work-in-emacs-and-iterm2-on-mac
;; Enable mouse support
(unless window-system
  (require 'mouse)
  (xterm-mouse-mode t)
  (global-set-key [mouse-4] '(lambda ()
                              (interactive)
                              (scroll-down 1)))
  (global-set-key [mouse-5] '(lambda ()
                              (interactive)
                              (scroll-up 1)))
  (defun track-mouse (e))
  (setq mouse-sel-mode t)
)

;; linear undo

(add-to-list 'load-path "~/.emacs.d")
(require 'undo-tree)
(global-undo-tree-mode)



;;(global-set-key (kbd "<f2>") 'previous-buffer)
;;(global-set-key (kbd "<f3>") 'next-buffer)

;; Emacs often generates a lot internal buffers that users are not interested in cycling thru. ⁖ {*scratch*, *Messages*, *shell*, *Shell Command Output*, *Occur*, *Completions*, *Apropos*, *info*, …}. You might define your own next-user-buffer that skips emacs's buffers, and you might define next-emacs-buffer that cycles thru just the emacs's buffers.
;; http://ergoemacs.org/emacs/emacs_buffer_management.html

(defun next-user-buffer ()
  "Switch to the next user buffer.
User buffers are those whose name does not start with *."
  (interactive)
  (next-buffer)
  (let ((i 0))
    (while (and (string-match "^*" (buffer-name)) (< i 50))
      (setq i (1+ i)) (next-buffer) )))

(defun previous-user-buffer ()
  "Switch to the previous user buffer.
User buffers are those whose name does not start with *."
  (interactive)
  (previous-buffer)
  (let ((i 0))
    (while (and (string-match "^*" (buffer-name)) (< i 50))
      (setq i (1+ i)) (previous-buffer) )))

(defun next-emacs-buffer ()
  "Switch to the next emacs buffer.
Emacs buffers are those whose name starts with *."
  (interactive)
  (next-buffer)
  (let ((i 0))
    (while (and (not (string-match "^*" (buffer-name))) (< i 50))
      (setq i (1+ i)) (next-buffer) )))

(defun previous-emacs-buffer ()
  "Switch to the previous emacs buffer.
Emacs buffers are those whose name starts with *."
  (interactive)
  (previous-buffer)
  (let ((i 0))
    (while (and (not (string-match "^*" (buffer-name))) (< i 50))
      (setq i (1+ i)) (previous-buffer) )))

(global-set-key (kbd "<f2>") 'previous-user-buffer)
(global-set-key (kbd "<f3>") 'next-user-buffer)
(global-set-key (kbd "<C-f2>") 'previous-emacs-buffer)
(global-set-key (kbd "<C-f3>") 'next-emacs-buffer)
(global-set-key (kbd "<f4>") 'bs-show)
(global-set-key [(control f2)] 'previous-emacs-buffer)



;; This will kill the current visible buffer without confirmation unless the buffer has been modified. In this last case, you have to answer y/n.
;; http://stackoverflow.com/a/6467602/588759
;; (global-set-key [(control x) (k)] 'kill-this-buffer)

;; http://stackoverflow.com/questions/437714/how-to-do-use-c-x-k-to-kill-an-emacs-buffer-opened-in-server-mode/443149#443149
(add-hook 'server-switch-hook
  (lambda ()
    (local-set-key (kbd "C-x k") '(lambda ()
                                    (interactive)
                                    (if server-buffer-clients
                                        (server-edit)
                                      (ido-kill-buffer))))))


;;(load "elisp/nxhtml/autostart.el")
;; http://stackoverflow.com/questions/5468952/how-do-i-hide-emacs-obsolete-variable-warnings
;; http://stackoverflow.com/questions/5468952/how-do-i-hide-emacs-obsolete-variable-warnings
;; http://stackoverflow.com/questions/3042123/in-emacs-how-to-stop-nxthml-to-mess-with-my-background-color

;; http://stackoverflow.com/a/11910853/588759
(load "elisp/multi-web-mode/multi-web-mode.el")
(load "elisp/multi-web-mode/mweb-example-config.el")


;;iswitchb
(iswitchb-mode 1)

;; http://stackoverflow.com/questions/2680545/ignore-certain-buffers-using-iswitchb
(add-to-list 'iswitchb-buffer-ignore "^ ")
;;(add-to-list 'iswitchb-buffer-ignore "*Messages*")
(add-to-list 'iswitchb-buffer-ignore "*ECB")
(add-to-list 'iswitchb-buffer-ignore "*Buffer")
(add-to-list 'iswitchb-buffer-ignore "*Completions")
(add-to-list 'iswitchb-buffer-ignore "*ftp ")
(add-to-list 'iswitchb-buffer-ignore "*bsh")
(add-to-list 'iswitchb-buffer-ignore "*jde-log")
(add-to-list 'iswitchb-buffer-ignore "^[tT][aA][gG][sS]$")

;; http://stackoverflow.com/questions/10164929/emacs-disable-messages-buffer
;;chyba nie działa
(setq message-log-max nil) ; disable messages buffer


;; “cut” and “copy” act on the current line if no text is visually selected
;; http://www.emacswiki.org/emacs/?action=browse;id=WholeLineOrRegion
(put 'kill-ring-save 'interactive-form
 '(interactive
   (if (use-region-p)
       (list (region-beginning) (region-end))
     (list (line-beginning-position) (line-beginning-position 2)))))
(put 'kill-region 'interactive-form
 '(interactive
   (if (use-region-p)
       (list (region-beginning) (region-end))
     (list (line-beginning-position) (line-beginning-position 2)))))

;; http://superuser.com/questions/124246/emacs-equivalent-to-vim-ci/346297#346297
(defun seek-backward-to-char (chr)
  "Seek backwards to a character"
  (interactive "cSeek back to char: ")
  (while (not (= (char-after) chr))
    (forward-char -1)))


(defun delete-between-pair (char)
  "Delete in between the given pair"
  (interactive "cDelete between char: ")
  (seek-backward-to-char char)
  (forward-char 1)
  (zap-to-char 1 char)
  (insert char)
  (forward-char -1))

(global-set-key (kbd "C-c i") 'delete-between-pair)

;; disable backups http://stackoverflow.com/questions/151945/how-do-i-control-how-emacs-makes-backup-files
;; http://anirudhsasikumar.net/blog/2005.01.21.html
;; http://www.emacswiki.org/emacs/AutoSave
;; http://stackoverflow.com/questions/1199216/emacs-dont-create-these-files-when-not-saving-modified-buffer
(setq make-backup-files nil)
(setq backup-inhibited t) ;disable backup
(setq auto-save-default nil) ;disable auto save, prevent emacs from autosaving to #file#

(setq toggle-case-fold-search t) ; search is case sensitive

(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; like in vim http://www.emacswiki.org/emacs/ShowParenMode
;; http://stackoverflow.com/questions/261522/what-is-the-command-to-match-brackets-in-emacs
;; http://emacs-fu.blogspot.com/2009/01/balancing-your-parentheses.html
(show-paren-mode 1)
(setq show-paren-delay 0)
(setq show-paren-style 'parenthesis) ; possible options are: parenthesis, mixed, expression
(set-face-background 'show-paren-match-face "#2A2AA1A19898")


;; spaces instead of tabs
;; http://emacsblog.org/2007/09/30/quick-tip-spaces-instead-of-tabs/
;; http://stackoverflow.com/a/7957258/588759
(setq-default c-basic-indent 4)
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)

;; sposób na załadowanie dla danego trybu?
(setq js2-mode-hook
  '(lambda () (progn
    (set-variable 'indent-tabs-mode nil))))

;; https://github.com/bbatsov/solarized-emacs
;;(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
;; nie widzę różnicy
;;(load-theme 'solarized-dark t)

; Fix copy-paste between emacs and other x programs
;; https://news.ycombinator.com/item?id=1655859
(setq x-select-enable-clipboard t)
(if (functionp 'x-cut-buffer-or-selection-value)
    (setq interprogram-paste-function 'x-cut-buffer-or-selection-value))


;; comment line or region
;; http://www.gnu.org/software/emacs/manual/html_node/emacs/Comment-Commands.html
;; http://stackoverflow.com/a/9697222/588759
(defun comment-or-uncomment-region-or-line ()
    "Comments or uncomments the region or the current line if there's no active region."
    (interactive)
    (let (beg end)
        (if (region-active-p)
            (setq beg (region-beginning) end (region-end))
            (setq beg (line-beginning-position) end (line-end-position)))
        (comment-or-uncomment-region beg end)))

;; http://www.emacswiki.org/emacs/CommentingCode
;; Original idea from
;; http://www.opensubscriber.com/message/emacs-devel@gnu.org/10971693.html
(defun comment-dwim-line (&optional arg)
  "Replacement for the comment-dwim command.
  If no region is selected and current line is not blank and we are not at the end of the line,
  then comment current line.
  Replaces default behaviour of comment-dwim, when it inserts comment at the end of the line."
    (interactive "*P")
    (comment-normalize-vars)
    (if (and (not (region-active-p)) (not (looking-at "[ \t]*$")))
        (comment-or-uncomment-region (line-beginning-position) (line-end-position))
      (comment-dwim arg)))

(global-set-key (kbd "M-;") 'comment-or-uncomment-region-or-line)
;;(global-set-key (kbd "M-;") 'comment-dwim-line)

(global-set-key (kbd "C-q") 'kill-region)
(global-set-key "\M-q" 'quoted-insert)
(global-set-key "\C-w" 'backward-kill-word)


;; redundant, because I can kill region with C-w
;; kill-region for an active (transient-mark-mode) region but backward-kill-word otherwise.
;; https://news.ycombinator.com/item?id=1654834
(defun backward-kill-word-or-kill-region (&optional arg)
  (interactive "p")
  (if (region-active-p)
      (kill-region (region-beginning) (region-end))
    (backward-kill-word arg)))

(global-set-key [(control backspace)] 'backward-kill-word-or-kill-region)


;; https://news.ycombinator.com/item?id=1655940
(global-set-key "\r" 'newline-and-indent)
;;(global-set-key "\C-j" 'newline)
;;  start a new line above or below the current line
(defun next-newline-and-indent ()
   (interactive)
   (end-of-line)
   (newline-and-indent))
;;(global-set-key [C-return] 'next-newline-and-indent)
(global-set-key "\C-j" 'next-newline-and-indent)
;; to start a new indented line above the current line, instead of Control a, Control o, Control i, I use Control Shift Return.
(defun prev-newline-and-indent ()
  (interactive)
  (beginning-of-line)
  (open-line 1)
  (indent-according-to-mode))
(global-set-key [C-S-j] 'prev-newline-and-indent)


;; don't sleep emacs accidentally
(global-set-key [(control z)] nil)

;; will show (row, column) in the mode line  -- e.g. (5, 20) is the 20th column of the 5th line
(column-number-mode)


;; nie działa ten binding
(global-set-key [(control meta %)] 'replace-regexp)

;; http://stray-notes.blogspot.com/2010/03/emacs-delete-to-start-of-line_22.html
;; or C-u 0 C-k or C-Spc C-a C-w or C-0 C-k (doesn't work)
(defun kill-start-of-line ()
  "kill from point to start of line"
  (interactive)
  (kill-line 0)
  (indent-according-to-mode))
;; nie działa ten bind
;; http://stackoverflow.com/questions/9856645/use-semicolon-in-global-set-key-for-function-in-emacs
(global-set-key (kbd "C-;") 'kill-start-of-line)

;; from https://github.com/technomancy/emacs-starter-kit
;; Use regex searches by default.
(global-set-key (kbd "C-s") 'isearch-forward-regexp)
(global-set-key (kbd "C-r") 'isearch-backward-regexp)
(global-set-key (kbd "M-%") 'query-replace-regexp)

;; vim like omnicompletion
;; http://stackoverflow.com/questions/1442231/vim-style-omnicomplete-for-emacs
;; http://company-mode.github.io/
;; M-x customize-group RET company
;;(add-hook 'after-init-hook 'global-company-mode)
(setq company-tooltip-limit 20)                      ; bigger popup window
(setq company-minimum-prefix-length 0)               ; autocomplete right after '.'
(setq company-idle-delay .3)                         ; shorter delay before autocompletion popup
(setq company-echo-delay 0)                          ; removes annoying blinking
(setq company-begin-commands '(self-insert-command)) ; start autocompletion only after typing

(custom-set-faces
 '(company-preview
   ((t (:foreground "darkgray" :underline t))))
 '(company-preview-common
   ((t (:inherit company-preview))))
 '(company-tooltip
   ((t (:background "lightgray" :foreground "black"))))
 '(company-tooltip-selection
   ((t (:background "steelblue" :foreground "white"))))
 '(company-tooltip-common
   ((((type x)) (:inherit company-tooltip :weight bold))
    (t (:inherit company-tooltip))))
 '(company-tooltip-common-selection
   ((((type x)) (:inherit company-tooltip-selection :weight bold))
    (t (:inherit company-tooltip-selection)))))

;; Can I complete by typing a key instead of waiting for the delay timer?
;; Yes, just bind company-manual-begin, company-complete-common, company-complete, company-select-next to a key in the global map.
;; http://www.emacswiki.org/emacs/CompanyMode
;;(global-set-key "\t" 'company-complete-common)

;; Would it be possible to make the down-case feature of dabbrev backend customizable as it is counter intuitive for example in shell-mode where it completes in lowercase all environment variables ? http://www.emacswiki.org/CompanyMode

;; http://stackoverflow.com/questions/4704748/emacs-completion-autocomplete-or-company
;; http://www.emacswiki.org/emacs/AutoComplete
;; http://www.emacswiki.org/emacs/CompanyMode

;; http://auto-complete.org/doc/manual.html
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")
(ac-config-default)
(setq ac-auto-start 0)
(setq ac-auto-show-menu 0.1)
